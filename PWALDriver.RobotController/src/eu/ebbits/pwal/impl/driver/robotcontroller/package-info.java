/**
 * Contains the implementation of the PWAL Driver for the robot controller used in M24 demo, updated for the PWAL 2.0.
 *
 * Copyright (c) 2015 Istituto Superiore Mario Boella. All Rights Reserved.
 *
 * @see eu.ebbits.pwal
 * 
 * @author    ISMB
 * @version   %I%, %G% 
 * @since     PWAL 0.1.0
 */
package eu.ebbits.pwal.impl.driver.robotcontroller;