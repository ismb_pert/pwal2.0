/**
 * Contains the implementation of the robot controller client used to control the robot controller.
 *
 * Copyright (c) 2015 Istituto Superiore Mario Boella. All Rights Reserved.
 *
 * @see eu.ebbits.pwal
 * 
 * @author    ISMB
 * @version   %I%, %G% 
 * @since     PWAL 0.1.0
 */
package eu.ebbits.pwal.impl.driver.robotcontroller.client;