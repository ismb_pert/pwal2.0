/*
 * Copyright 2015 Istituto Superiore Mario Boella
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package eu.ebbits.pwal.api.driver.robotcontroller;

import eu.ebbits.pwal.api.driver.PWALDriver;


/**
 * Interface of the PWAL Driver for the robot controller used in M24 demo, updated for the PWAL 2.0.
 *
 *
 * @author    ISMB
 * @version    %I%, %G%
 * @since    M36demo 1.0
 * 
 */
public interface RobotControllerDriver extends PWALDriver {
    
    /**
     * @return the host
     */
    String getHost();

    /**
     * @param host the host to set
     */
    void setHost(String host);

    /**
     * @return the port
     */
    int getPort();

    /**
     * @param port the port to set
     */
    void setPort(Integer port);
}
