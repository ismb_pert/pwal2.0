/**
 *Contains the interface of the PWAL Driver for the RobotController used in M24 demonstrator.
 *
 * Copyright (c) 2015 Istituto Superiore Mario Boella. All Rights Reserved.
 *
 * @see eu.ebbits.pwal
 * 
 * @author    ISMB
 * @version   %I%, %G% 
 * @since     PWAL 0.1.0
 */
package eu.ebbits.pwal.api.driver.robotcontroller;