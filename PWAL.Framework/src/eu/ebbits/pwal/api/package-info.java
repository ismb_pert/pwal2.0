/**
 * Provides the application program interface (API) necessary to control the
 * Physical World Adaptation Layer (PWAL) drivers and expose underlying 
 * variables, services and events through it.
 * <p>
 * The PWAL API is composed of:
 * <ul>
 * <li>An annotations package (see @link eu.ebbits.pwal.api.annotations) with 
 * the provided types of <code>PWALAnnotation</code>s
 * 
 * <li>A driver package (see @link eu.ebbits.pwal.api.driver) with 
 * several interfaces needed to interact with the <code>PWALDriver</code>
 * 
 * <li>An exceptions package (see @link eu.ebbits.pwal.api.exceptions) with 
 * supported types of <code>PWALExceptions/code>s
 * 
 * <li>A model package (see @link eu.ebbits.pwal.api.model) with the 
 * different <code>PWALObject</code>s used by the PWAL.
 * 
 * </ul>
 * <p> 
 * Copyright (c) 2015 Istituto Superiore Mario Boella. All Rights Reserved.
 *
 * @see eu.ebbits.pwal
 * 
 * @author    ISMB
 * @version   %I%, %G% 
 * @since     PWAL 0.1.0
 * 
 */
package eu.ebbits.pwal.api;
