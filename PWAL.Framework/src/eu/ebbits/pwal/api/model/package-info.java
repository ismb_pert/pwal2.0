/**
 * Provides the different models of the physical world basic elements abstracted
 * by the adaptation layer.
 * <p>
 * Copyright (c) 2015 Istituto Superiore Mario Boella. All Rights Reserved.
 *
 * @see eu.ebbits.pwal.api
 * @since PWAL 0.1.0
 */
package eu.ebbits.pwal.api.model;
