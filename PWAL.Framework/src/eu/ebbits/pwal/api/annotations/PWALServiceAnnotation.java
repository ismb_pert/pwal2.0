/*
 * Copyright 2015 Istituto Superiore Mario Boella
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package eu.ebbits.pwal.api.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * This annotation is used in conjunction with 
 * {@link ReflectiveServicesDelegate}. If a method of a reflective delegate is 
 * annotated with the exposed attribute set to "true", it will be exposed with 
 * the PWAL.
 *
 * @author	 ISMB
 * @version	%I%, %G%
 * @see	    eu.ebbits.pwal.impl.driver.framework.ReflectiveServicesDelegate
 * @since	  PWAL 0.1.0
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface PWALServiceAnnotation {

	/** 
	 * Checks whether the <code>PWALEvent</code> has been annotated as exposed.
	 *
	 * @since		PWAL 0.1.0
	 */
	boolean exposed();
}
