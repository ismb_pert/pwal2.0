/**
 * This package should include all Annotations which can be handled internally 
 * by the PWAL.
 * Annotations are typically used inside the PWAL to support reflective 
 * delegates.
 * 
 * For example, if a driver developer designs its driver to have 5 methods, 
 * by using annotations he/she can "tag" with annotations e.g. 5 methods 
 *  which are directly exposed to the PWAL without adding any special 
 * logic.
 * 
 * Copyright (c) 2015 Istituto Superiore Mario Boella. All Rights Reserved.
 *
 * @see eu.ebbits.pwal.api
 * 
 * 
 * @since PWAL 0.1.0
 */
package eu.ebbits.pwal.api.annotations;
