/*
 * Copyright 2015 Istituto Superiore Mario Boella
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package eu.ebbits.pwal.api.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * This annotation is currently unimplemented (It has been added just as 
 * place-holder and should not be used).
 * NOTE: Current considered use-cases do not request this kind of "second-order"
 * reflective logic.
 *
 *
 * @author	 ISMB
 * @version	%I%, %G%
 * @see	    eu.ebbits.pwal.impl.driver.PWALDriver
 * @since	  PWAL 0.1.0
 */
@Deprecated
@Retention(RetentionPolicy.RUNTIME)
public @interface PWALServiceMetaAnnotation {

}