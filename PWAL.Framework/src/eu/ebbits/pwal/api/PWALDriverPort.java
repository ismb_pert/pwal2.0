/*
 * Copyright 2015 Istituto Superiore Mario Boella
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package eu.ebbits.pwal.api;

import eu.ebbits.pwal.api.driver.PWALDriver;

/**
 * <code>PWALDriverPort</code> service interface.
 * <p> 
 *
 * @author	ISMB
 * @version	%I%, %G%
 * @since	PWAL 0.2.0
 */
public interface PWALDriverPort {

	/**
	 * Checks whether a Driver is registered or not.
	 * 
	 * @param driver - the PWALDriver to check.
	 * 
	 * @return		<code>True</code> if the Driver is loaded,
	 * 				<code>False</code> otherwise.
	 * 
	 * @since		PWAL 2.0
	 */
	boolean isDriverRegistered(PWALDriver driver);

	/**
	 * Registers a Driver.
	 * 
	 * @param driver - the PWALDriver to register.
	 * 
	 * @since		PWAL 0.2.0
	 */
	void registerDriver(PWALDriver driver);

	/**
	 * Unregisters a Driver.
	 * 
	 * @param driver - the PWALDriver to unregister.
	 * 
	 * @since		PWAL 0.2.0
	 */
	void unregisterDriver(PWALDriver driver);

}
