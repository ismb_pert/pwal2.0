/*
 * Copyright 2015 Istituto Superiore Mario Boella
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package eu.ebbits.pwal.api.driver;

import eu.ebbits.pwal.api.model.PWALControlEvent;

/**
 * Interface for receiving and processing events generated from the 
 * <code>PWALDriver</code>.
 * <p> 
 *
 * @author		ISMB
 * @version		%I%, %G%
 * @see 		eu.ebbits.pwal.api.driver.PWALDriver
 * @since		PWAL 0.1.0
 */
public interface PWALDelegateSubscriber {

	/** 
	* Processes the driver started event.
	*
	* @param e - 	the <code>PWALDriverControlEvent</code> to process
	*  
	* @see			eu.ebbits.pwal.api.model.PWALControlEvent
	* @since		PWAL 0.1.0
	*/
	void driverStarted(PWALControlEvent e);
	
	/** 
	* Processes the driver stopped event.
	*
	* @param e -	the <code>PWALDriverControlEvent</code> to process
	*  
	* @see			eu.ebbits.pwal.api.model.PWALControlEvent
	* @since		PWAL 0.1.0
	*/
	void driverStopped(PWALControlEvent e);
	
	/** 
	* Processes driver debug events.
	*
	* @param e - 	the <code>PWALDriverControlEvent</code> to process
	*  
	* @see			eu.ebbits.pwal.api.model.PWALControlEvent
	* @since		PWAL 0.1.0
	*/
	void driverDebug(PWALControlEvent e);
	
	/** 
	* Processes driver information events.
	*
	* @param e -	the <code>PWALDriverControlEvent</code> to process
	*  
	* @see			eu.ebbits.pwal.api.model.PWALControlEvent
	* @since		PWAL 0.1.0
	*/
	void driverInfo(PWALControlEvent e);
	
	/** 
	* Processes driver warning events.
	*
	* @param e -	the <code>PWALDriverControlEvent</code> to process
	*  
	* @see			eu.ebbits.pwal.api.model.PWALControlEvent
	* @since		PWAL 0.1.0
	*/
	void driverWarning(PWALControlEvent e);
	
	/** 
	* Processes driver error events.
	*
	* @param e -	the <code>PWALDriverControlEvent</code> to process
	*  
	* @see			eu.ebbits.pwal.api.model.PWALControlEvent
	* @since		PWAL 0.1.0
	*/
	void driverError(PWALControlEvent e);
	
	/** 
	* Processes driver critical error events.
	*
	* @param e -	the <code>PWALDriverControlEvent</code> to process
	*  
	* @see			eu.ebbits.pwal.api.model.PWALControlEvent
	* @since		PWAL 0.1.0
	*/
	void driverCriticalError(PWALControlEvent e);
	
}
