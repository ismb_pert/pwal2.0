/**
 * Provides the <code>PWALDriver</code> core interface and delegates interfaces
 * <p>
 * The driver projects inherit from this interfaces to create their basic common architecture 
 * <p> 
 * Copyright (c) 2015 Istituto Superiore Mario Boella. All Rights Reserved.
 *
 * @see eu.ebbits.pwal.api
 * @since PWAL 0.1.0
 */
package eu.ebbits.pwal.api.driver;
