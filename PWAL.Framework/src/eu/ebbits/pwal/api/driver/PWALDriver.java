/*
 * Copyright 2015 Istituto Superiore Mario Boella
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package eu.ebbits.pwal.api.driver;

/**
* Generic interface for PWAL drivers.
*
* <p> 
*
* @author		ISMB
* @version		%I%, %G%
* @see 		PWALEventsDelegate
* @see 		PWALServicesDelegate
* @see 		PWALVariablesDelegate
* @see 		PWALDelegate
* 
* @since		PWAL 0.2.0
*/
public interface PWALDriver extends PWALDelegate {
	
	/**
	 * Retrieves the driver id.
	 * 
	 * @return 		Driver unique identification
	 * 
	 * @since		PWAL 0.2.0
	 */
	String getDriverID();
	
	/**
	 * Retrieves the driver name.
	 * 
	 * @return 		Driver name that describe the driver
	 * 
	 * @since		PWAL 0.2.0
	 */
	String getDriverName();
	
	/**
	 * Retrieves the drivers version.
	 * 
	 * @return Version of the driver
	 * 
	 * @since		PWAL 0.2.0
	 */
	String getDriverVersion();

	/**
	 * Retrieves the Events delegate.
	 * 
	 * @return		instance of <code>PWALEventsDelegate</code> used by the driver to manage the events
	 * 
	 * @since		PWAL 0.2.0
	 */
	PWALEventsDelegate getEventsDelegate();
	
	/**
	 * Retrieves the Services delegate.
	 * 
	 * @return 		instance of <code>PWALEventsDelegate</code> used by the driver to manage the services
	 * 
	 * @since		PWAL 0.2.0
	 */
	PWALServicesDelegate getServicesDelegate();
	
	/**
	 * Retrieves the Variables delegate.
	 * 
	 * @return 		instance of <code>PWALVariablesDelegate</code> used by the driver to manage the variables
	 * 
	 * @since		PWAL 0.2.0
	 */
	PWALVariablesDelegate getVariablesDelegate();
}
