/**
 * This package should include all public Exceptions which can be handled 
 * internally by the PWAL.
 * 
 * Copyright (c) 2015 Istituto Superiore Mario Boella. All Rights Reserved.
 *
 * @see eu.ebbits.pwal.api
 * @since PWAL 0.1.0
 */
package eu.ebbits.pwal.api.exceptions;
