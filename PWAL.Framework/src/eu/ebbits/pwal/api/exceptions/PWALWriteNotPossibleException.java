/*
 * Copyright 2015 Istituto Superiore Mario Boella
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package eu.ebbits.pwal.api.exceptions;

/**
 * PWAL Exception to mark impossibility to write a variable.
 * <p> 
 *
 * @author	 ISMB
 * @version	%I%, %G%
 * @since	  PWAL 0.1.0
 */
public class PWALWriteNotPossibleException extends PWALException {

	/**
	 * Unique identification for serialization.
	 */
	private static final long serialVersionUID = 3075573176642849477L;
	
	//TODO Idea for future releases: support reason of not possibility 
	//(e.g. read-only, ..I/O error...) 

}
