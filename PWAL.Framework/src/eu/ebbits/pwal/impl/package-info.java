/**
 * Contains the source code of the PWAL framework behind the API, as well as the 
 * technology-specific driver extensions/implementations.
 * <p>
 * Copyright (c) 2015 Istituto Superiore Mario Boella. All Rights Reserved.
 *
 * @see eu.ebbits.pwal
 * @since PWAL 0.1.0
 */
package eu.ebbits.pwal.impl;
