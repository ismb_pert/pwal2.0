/**
 * This package is imagined as part of the new ebbits DDK: it will thus undergo significant modifications in the ebbits iterative process. 
 * it will be thus documented during the final release after all needed adapters are available.
 * 
 * Copyright (c) 2015 Istituto Superiore Mario Boella. All Rights Reserved.
 *
 * @author	 ISMB
 * @version	%I%, %G%
 * @see	    eu.ebbits.pwal.impl.driver.PWALDriverImpl
 * @since	  PWAL 0.1.0
 */
package eu.ebbits.pwal.impl.driver;
