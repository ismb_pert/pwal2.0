/*
 * Copyright 2015 Istituto Superiore Mario Boella
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package eu.ebbits.pwal.impl.driver;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import eu.ebbits.pwal.api.driver.PWALDelegate;
import eu.ebbits.pwal.api.driver.PWALDelegateSubscriber;
import eu.ebbits.pwal.api.driver.PWALDriver;
import eu.ebbits.pwal.api.model.PWALControlEvent;

/**
 * This class is imagined as part of the new ebbits DDK: it will thus undergo significant modifications in the ebbits iterative process. 
 * it will be thus documented during the final release after all needed adapters are available.
 * 
 *
 * @author		ISMB
 * @version		%I%, %G%
 * @see			eu.ebbits.pwal.impl.driver.PWALDriverImpl
 * @since		PWAL 0.1.0
 */
public abstract class PWALDelegateImpl implements PWALDelegate {

	protected PWALDriver driver;
	protected Collection<PWALDelegateSubscriber> subscribers;
	protected Thread thread;

	public PWALDelegateImpl() {
		this.subscribers = new HashSet<PWALDelegateSubscriber>();
		this.thread = new Thread(this);
	}

	public PWALDelegateImpl(PWALDriver driver) {
		this();
		this.driver = driver;
	}

	public final void configure(Map<String, Object> parameters) {
		for (Entry<String, Object> param : parameters.entrySet()) {
			try {
				Vector<Object> values = new Vector<Object>();
				values.add(param.getValue());
				configure(param.getKey(), values);
			} catch (Exception e) {}
		}
	}
	
	public final void configure(String methodName, Vector<Object> values) {
		try {
			this.getClass().getMethod(methodName,values.getClass()).invoke(this, values);
		} catch (Exception e) {
			System.err.println("Unable to configure using method "+methodName+", error: "+ (e.getMessage()==null?"invalid parameters":e.getMessage()));
			this.signalWarningEvent(new PWALControlEvent("Could not " + methodName, e));
		}		
	}
	

	public final void configureParam(String methodName, Object argValue) {
		try {
			this.getClass().getMethod(methodName, argValue.getClass()).invoke(this, argValue);
		} catch (Exception e) {
			System.err.println("Unable to configure using method "+methodName+", error: "+ (e.getMessage()==null?e.getMessage():"invalid parameters"));
			this.signalWarningEvent(new PWALControlEvent("Could not " + methodName, e));
		}
	}
	
	public final PWALDriver getDriver() {
		return this.driver;
	}

	public final void setDriver(PWALDriver driver) {
		this.driver = driver;
	}

	public boolean isStarted() {
		return this.thread.isAlive();
	}

	public void start() {
		this.thread.start();
	}

	public void stop() {
		this.thread.interrupt();
	}

	synchronized final public void subscribe(PWALDelegateSubscriber s) {
		this.subscribers.add(s);
	}

	synchronized final public void unsubscribe(PWALDelegateSubscriber s) {
		this.subscribers.remove(s);
	}

	synchronized final public boolean isSubscribed(PWALDelegateSubscriber s) {
		return this.subscribers.contains(s);
	}

	synchronized protected void signalDebugEvent(PWALControlEvent e) {
		PWALControlEvent ev = new PWALControlEvent(e);
		ev.setType(PWALControlEvent.ControlEventType.DEFAULT_DEBUG);
		for(PWALDelegateSubscriber s : this.subscribers) {
			s.driverDebug(ev);
		}
	}

	synchronized protected void signalWarningEvent(PWALControlEvent e) {
		PWALControlEvent ev = new PWALControlEvent(e);
		ev.setType(PWALControlEvent.ControlEventType.DEFAULT_WARNING);
		for(PWALDelegateSubscriber s : this.subscribers) {
			s.driverWarning(ev);
		}
	}

	synchronized protected void signalErrorEvent(PWALControlEvent e) {
		PWALControlEvent ev = new PWALControlEvent(e);
		ev.setType(PWALControlEvent.ControlEventType.DEFAULT_ERROR);
		for(PWALDelegateSubscriber s : this.subscribers) {
			s.driverError(ev);
		}
	}

	synchronized protected void signalCriticalEvent(PWALControlEvent e) {
		PWALControlEvent ev = new PWALControlEvent(e);
		ev.setType(PWALControlEvent.ControlEventType.DEFAULT_CRITICAL);
		for(PWALDelegateSubscriber s : this.subscribers) {
			s.driverCriticalError(ev);
		}
			
	}

	synchronized protected void signalInfoEvent(PWALControlEvent e) {
		PWALControlEvent ev = new PWALControlEvent(e);
		ev.setType(PWALControlEvent.ControlEventType.DEFAULT_INFO);
		for(PWALDelegateSubscriber s : this.subscribers) {
			s.driverInfo(ev);
		}
	}
}
